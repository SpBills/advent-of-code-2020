use std::fs;

pub fn run() {
    let string_boarding_pass =
        fs::read_to_string("/home/spencer/prg/rust/advent-of-code-2020/inputs/d5.txt").unwrap();

    let binary_boarding_pass = convert_to_binary(string_boarding_pass);

    let largest = find_largest(binary_boarding_pass);

    println!("Largest boarding pass: {:?}", largest);
}

#[derive(Debug)]
struct BoardingPass {
    row: u32,
    col: u32,
}

fn convert_to_binary(boarding_passes: String) -> Vec<BoardingPass> {
    boarding_passes
        .lines()
        .map(|s| {
            let bin = s
                .as_bytes()
                .iter()
                .map(|encoded| {
                    let character = *encoded as char;
                    if character == 'B' || character == 'R' {
                        '1'
                    } else {
                        '0'
                    }
                })
                .collect::<String>();

            BoardingPass {
                row: u32::from_str_radix(&bin[0..7], 2).unwrap(),
                col: u32::from_str_radix(&bin[7..bin.len()], 2).unwrap(),
            }
        })
        .collect()
}

fn find_largest(binary_boarding_passes: Vec<BoardingPass>) -> u32 {
    binary_boarding_passes.iter().map(|s| s.row * 8 + s.col).max().unwrap()
}
