use std::fs;

pub fn run() {
    let parsed_values = parse();

    part_1(parsed_values);
}

fn parse() -> Vec<(String, i32)> {
    let input_string =
        fs::read_to_string("/home/spencer/prg/rust/advent-of-code-2020/inputs/d8.txt").unwrap();

    input_string
        .lines()
        .map(|line| {
            let mut split_string = line.split(" ");
            let key: String = split_string.nth(0).unwrap().to_string();
            let num: i32 = split_string.nth(0).unwrap().parse().unwrap();
            (key, num)
        })
        .collect()
}

enum InstructionMessage {
    ACC,
    JMP,
    NOP,
    WNG
}

struct Instruction {
    name: String,
    inst: InstructionMessage
}

impl InstructionMessage {
    fn new(name: String) -> Instruction {
        match name.as_str() {
            "acc" => Instruction {name, inst: InstructionMessage::ACC},
            "jmp" => Instruction {name, inst: InstructionMessage::JMP},
            "nop" => Instruction {name, inst: InstructionMessage::NOP},
            _ => Instruction {name, inst: InstructionMessage::WNG}
        }
    }
}

fn part_1(input: Vec<(String, i32)>) {
    let mut acc: i32 = 0;
    let mut iter: i32 = 0;

    let mut ran_iters: Vec<i32> = Vec::new();

    loop {
        let current_instruction = input.get(iter as usize).unwrap().to_owned();
        let instruction_type = InstructionMessage::new(current_instruction.0);
        if ran_iters.contains(&iter) {
            println!("{}", acc);
            break;
        }
        ran_iters.push(iter);

        match instruction_type.inst {
            InstructionMessage::ACC => {
                acc += current_instruction.1;
                iter += 1;
            },
            InstructionMessage::JMP => {
                iter += current_instruction.1;
            },
            InstructionMessage::NOP => {
                iter += 1;
            },
            _ => println!("Something went terribly wrong.")
        }
    }
}
