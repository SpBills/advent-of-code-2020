use std::{
    collections::{HashMap, HashSet, VecDeque},
    fs,
};

use regex::Regex;

pub fn run() {
    let parsed_data: HashMap<String, HashMap<String, u32>>= parse();

    println!("{:?}", parsed_data);
}

fn parse() -> HashMap<String, HashMap<String, u32>> {
    let input_data =
        fs::read_to_string("/home/spencer/prg/rust/advent-of-code-2020/inputs/d7.txt").unwrap();

    let bag: HashMap<String, HashMap<String, u32>> = input_data
        .lines()
        .map(|line| {
            let mut first_split = line.split("contain");
            let second_split = first_split.clone().nth(1).unwrap().split(",");

            let inside_maps: HashMap<String, u32> = second_split
                .filter_map(|inside_bag| {
                    let digit_pattern = Regex::new(r"\d+").unwrap();
                    let character_pattern = Regex::new(r"[.\d]+").unwrap();

                    if let Some(digits) = digit_pattern.captures(inside_bag) {
                        let confirmed_digits = digits.get(0).unwrap().as_str().parse().unwrap();
                        let chars = character_pattern
                            .replace_all(inside_bag, "")
                            .trim_start_matches(char::is_whitespace)
                            .to_string();
                        Some((chars, confirmed_digits))
                    } else {
                        None
                    }
                })
                .collect();

            (
                first_split
                    .nth(0)
                    .unwrap()
                    .trim_end_matches(char::is_whitespace)
                    .to_string(),
                inside_maps,
            )
        })
        .collect();

    bag
}
