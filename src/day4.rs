use std::collections::HashMap;
use std::fs;

pub fn run() {
    let valid_passports = part_1();

    println!("{} valid passports.", valid_passports);
}

#[derive(Debug)]
struct Passport {
    byr: String,
    iyr: String,
    eyr: String,
    hgt: String,
    hcl: String,
    ecl: String,
    pid: String,
    cid: Option<String>,
}

fn part_1() -> u32 {
    let file_content =
        fs::read_to_string("/home/spencer/prg/rust/advent-of-code-2020/inputs/d4.txt").unwrap();

    let passport_data = file_content.split("\n\n");

    let pport_vec: Vec<Option<Passport>> = passport_data
        .clone()
        .map(|p| {
            let split_data = p.split_whitespace();

            let mut passport_data_map: HashMap<String, String> = HashMap::new();

            for data in split_data {
                let mut key_value_split = data.split(":");
                passport_data_map.insert(
                    key_value_split.nth(0).unwrap().to_string(),
                    key_value_split.last().unwrap().to_string(),
                );
            }

            return Some(Passport {
                byr: passport_data_map.get("byr")?.clone(),
                iyr: passport_data_map.get("iyr")?.clone(),
                eyr: passport_data_map.get("eyr")?.clone(),
                hgt: passport_data_map.get("hgt")?.clone(),
                hcl: passport_data_map.get("hcl")?.clone(),
                ecl: passport_data_map.get("ecl")?.clone(),
                pid: passport_data_map.get("pid")?.clone(),
                cid: passport_data_map
                    .get("cid")
                    .and(None)
                    .clone(),
            });
        })
        .collect();

    let final_res: Vec<&Option<Passport>> = pport_vec.iter().filter(|f| !f.is_none()).collect();

    return final_res.len() as u32;
}

fn part_2() {

}