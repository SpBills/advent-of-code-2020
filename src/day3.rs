use std::fs;

pub fn run() {
    let part2 = parse(3, 1);

    println!("{}", (part2))
}

pub fn parse(right: u8, down: u8) -> u8 {
    let mut coordinates: (u8, u8) = (0, 0);

    let mut hit: u8 = 0;

    let string = fs::read_to_string("/home/spencer/prg/rust/advent-of-code-2020/inputs/d3.txt").unwrap();
    let lines: Vec<&str> = string.lines().collect();
    while coordinates.1 + 1 < lines.len() as u8 {
        coordinates.0 += right;
        coordinates.1 += down;

        let line = lines.get(coordinates.1 as usize).unwrap();
        if line.as_bytes()[(coordinates.0 % (lines.get(coordinates.1 as usize).unwrap().len()) as u8) as usize] as char == '#' {
            hit += 1;
        }
    }

    return hit
}