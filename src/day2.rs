pub fn run() {
    let passwords = parse();

    let final_count = check_string_part_1(&passwords);

    let mut reg = 0;
    for password in final_count {
        if password {reg += 1};
    }
    println!("{:?}", reg);

    let final_count_2 = check_string_part_2(passwords);

    let mut reg_2 = 0;
    for password in final_count_2 {
        if password {reg_2 += 1};
    }
    
    println!("{:?}", reg_2);
}


#[derive(Debug)]
pub struct Password {
    occurence: u32,
    max: u32,
    letter: String,
    full_password: String
}

pub fn parse() -> Vec<Password> {
    let input = std::fs::read_to_string("/home/spencer/prg/rust/aoc2020/one/inputs/d2.txt").expect("Something went wrong reading the file.");

    let passwords: Vec<Password> = input.lines().map(|pass| {
        let mut first = pass.split("-");
        let mut second = first.clone().nth(1).unwrap().split(":");
        let mut third =  second.clone().nth(0).unwrap().split(" ");

        let occurence = first.nth(0).unwrap().parse::<u32>().unwrap();
        let max = third.clone().nth(0).unwrap().parse::<u32>().unwrap();
        let letter = third.nth(1).unwrap().to_string();
        let full_password = second.nth(1).unwrap().to_string();

        return Password {
            occurence,
            max,
            letter,
            full_password
        }
    }).collect();

    passwords
}

pub fn check_string_part_1(passwords: &Vec<Password>) -> Vec<bool> {
    let amount_valid = passwords.iter().map(|password| {
        let matches: Vec<_> = password.full_password.match_indices(&password.letter).collect();

        return matches.len() as u32 <= password.max && matches.len() as u32 >= password.occurence;
    }).collect::<Vec<bool>>();

    amount_valid
}

pub fn check_string_part_2(passwords: Vec<Password>) -> Vec<bool> {
    let amount_valid = passwords.iter().map(|password| {
        let first = password.full_password.as_bytes()[password.occurence as usize] as char == password.letter.as_bytes()[0] as char;
        let second = password.full_password.as_bytes()[password.max as usize] as char == password.letter.as_bytes()[0] as char;

        return first ^ second
    }).collect::<Vec<bool>>();

    amount_valid
}