use std::{fs, str::Split};

pub fn run() {
    let expenses_file =
        fs::read_to_string("/home/spencer/prg/rust/aoc2020/one/inputs/d1.txt").unwrap();
    let expenses = expenses_file.split("\n");
    if let Some(final_exp) = parse_expenses(parse_expenses_to_i32(expenses)) {
        println!("Found 3 numbers that add up to 2020. All terms multiplied: {}", final_exp)
    } else {
        println!("None found.")
    };
}

fn parse_expenses_to_i32(expenses: Split<&str>) -> Vec<i32> {
    expenses
        .map(|e| {
            e.parse::<i32>()
                .expect("Encountered error parsing expenses.")
        })
        .collect()
}

fn parse_expenses(expenses: Vec<i32>) -> Option<i32> {
    for a in expenses.iter() {
        for b in expenses.iter() {
            for c in expenses.iter() {
                if a + b + c == 2020 {
                    return Some(a * b * c);
                }
            }
        }
    }

    return None;
}