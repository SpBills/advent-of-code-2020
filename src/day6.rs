use std::fs;

pub fn run() {
    let input_string =
        fs::read_to_string("/home/spencer/prg/rust/advent-of-code-2020/inputs/d6.txt").unwrap();

    let qs = QuestionSheet::new(input_string);

    let part_1 = part_1(qs);

    println!("{:?}", part_1);

}

#[derive(Debug, Clone)]
struct Person {
    answers: Vec<char>,
}

#[derive(Debug, Clone)]
struct Group {
    people: Vec<Person>,
    answered_in_group: Vec<char>,
    part_2: usize,
}

#[derive(Debug, Clone)]
struct QuestionSheet {
    groups: Vec<Group>,
}

impl QuestionSheet {
    fn new(data_sheet: String) -> QuestionSheet {
        let groups: Vec<Group> = data_sheet
            .split("\n\n")
            .map(|g| Group::new(g.to_string()))
            .collect();

        QuestionSheet { groups }
    }
}

impl Group {
    fn new(group_data: String) -> Group {
        let people: Vec<Person> = group_data
            .lines()
            .map(|p| Person::new(p.to_string()))
            .collect();

        Group {
            people,
            answered_in_group: Vec::new(),
            part_2: 0,
        }
    }
}

impl Person {
    fn new(person_data: String) -> Person {
        let answers = person_data.as_bytes().iter().map(|a| *a as char).collect();
        Person { answers }
    }
}

fn part_1(qs: QuestionSheet) -> usize {
    let groups: Vec<Group> = qs
        .groups
        .into_iter()
        .map(|mut group| {
            group.clone().people.iter().for_each(|person| {
                person.answers.iter().for_each(|a| {
                    if !group.answered_in_group.contains(a) {
                        group.answered_in_group.push(*a);
                    }
                })
            });

            group
        })
        .collect();

    let new_qs = QuestionSheet { groups };

    new_qs
        .groups
        .iter()
        .map(|f| f.answered_in_group.len())
        .sum()
}